// Cargar modulos y crear nueva aplicacion
var express = require("express");

// Cosas de Express
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // soporte para bodies codificados en jsonsupport
app.use(bodyParser.urlencoded({
  extended: true
})); // soporte para bodies codificados

/* Permite CORS */
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Fichero donde haremos las rutas y las atenderemos
var routes = require("./routes/routes.js");


//Llamamos al fichero de rutas pasandole el app
routes(app);

// Ponemos a funcionar el servidor Web de express
var puerto = 8081;
var server = app.listen(puerto, function () {
  console.log('El servidor está funcionando en el puerto: '+puerto);
});