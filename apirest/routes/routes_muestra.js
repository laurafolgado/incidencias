//Basado en ejemplo de https://www.codementor.io/wapjude/creating-a-simple-rest-api-with-expressjs-in-5min-bbtmk51mq

//Necesitamos para poder hacer hash 512
var sha512 = require('js-sha512');

var appRouter = function (app) {

var mysql = require('mysql');

// Creo un pool de conexiones MySQl. Esto nos permitira tener varias conexiones pre-creadas para atender rápido
var pool = mysql.createPool({
    connectionLimit: 100, //important
    host: '172.17.0.2', //ip del servidor postgresql
    user: 'admin',
    password: 'Abcd_1234',
    database: 'incidencias',
    debug: false
});
    
// Configurar cabeceras y cors -para evitar errores con app.delete-
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

// GET Raiz
app.get('/',(req,res) =>{
    console.log("Accede a raiz");
}) ;


// TODOS LOS ROLES --> NO SE USA
/* app.get('/roles', (req, res) => {
    pool.query('SELECT * FROM roles', (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Todos los roles /roles")
        } else {
            console.log("ERROR: Todos los roles /roles"+err);
        }
    });  
});

// TODOS LOS TIPOS
app.get('/tipos', (req, res) => {
    pool.query('SELECT * FROM tipos', (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Todos los tipos /tipos")
        } else {
            console.log("ERROR: Todos los tipos /tipos "+err);
        }
    });  
});

// TIPO POR IDTIPO
app.get('/tipo/:idtipo', (req, res) => {
    var idtipo = req.params.idtipo;
    pool.query('SELECT * FROM tipos where idtipo = '+mysql.escape(idtipo), (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Tipo por idtipo /tipo/:idtipo")
        } else {
            console.log("ERROR :Tipo por idtipo /tipo/:idtipo "+err);
        }
    });  
});
    

// TODAS LAS ÁREAS
app.get('/areas', (req, res) => {
    pool.query('SELECT * FROM areas', (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Todas las áreas /areas ")
        } else {
            console.log("ERROR: Todas las áreas /areas "+err);
        }
    });  
});

// TODAS LAS ÁREAS A PARTIR DE UN SLUG
app.get('/areas/:slug', (req, res) => {
    var slug = req.params.slug;
    var consulta = 'SELECT a.* FROM areas a, tipos t where a.idtipo = t.idtipo and t.slug=' + mysql.escape(slug);
    console.log(consulta);
    pool.query(consulta, (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Todas las áreas a partir de un slug /areas/:slug")
        } else {
            console.log("ERROR: Todas las áreas a partir de un slug /areas/:slug "+err);
        }
    });  
});    

// UN ÁREA A PARTIR DE SU IDENTIFICADOR
app.get('/area/:idarea', (req, res) => {
    var consulta = 'SELECT * FROM areas where idarea =' + mysql.escape(req.params.idarea);
    console.log(consulta);
    pool.query(consulta, (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Un área a partir de su identificador /areas/:idarea")
        } else {
            console.log("ERROR: Un área a partir de su identificador /areas/:idarea "+err);
        }
    });  
});    

// AREAS Y TIPOS
app.get('/areastipos', (req, res) => {
    var consulta = 'select a.idarea, a.area, a.idtipo, t.tipo FROM areas a, tipos t WHERE a.idtipo = t.idtipo';
    console.log(consulta);
    pool.query(consulta, (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Areas y tipos /areastipos")
        } else {
            console.log("ERROR: Areas y tipos /areastipos "+err);
        }
    });  
});  


// TODOS LOS EVENTOS
app.get('/eventos', (req, res) => {
    pool.query('SELECT e.idevento, e.titulo, e.descripcion, e.fechaini, e.fechafin, e.hora, e.idarea, e.idusuario, e.obligatorio, a.area, u.usuario FROM eventos e, areas a, usuarios u WHERE e.idarea = a.idarea AND e.idusuario = u.idusuario ORDER BY e.fechaini ASC, e.hora ASC', (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Todos los eventos /eventos")
        } else {
            console.log("ERROR: Todos los eventos /eventos "+err);
        }
    });  
});

// TODOS LOS EVENTOS CON SU AREA
app.get('/eventosarea', (req, res) => {
    pool.query('SELECT e.idevento, e.titulo, e.descripcion, e.fechaini, e.fechafin, e.hora, e.idarea, e.idusuario, e.obligatorio, a.area FROM eventos e, areas a WHERE e.idarea = a.idarea ORDER BY e.fechaini ASC, e.hora ASC', (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Todos los eventos con su área /eventosarea")
        } else {
            console.log("ERROR: Todos los eventos con su área /eventosarea "+err);
        }
    });  
});

// GET Todos los eventos OBLIGATORIOS
/* app.get('/eventos/obligatorios', (req, res) => {
    pool.query('SELECT e.idevento, e.titulo, e.descripcion, e.fechaini, e.fechafin, e.hora, e.idarea, e.usuario, e.obligatorio FROM eventos e WHERE e.obligatorio = 1 ORDER BY e.fechaini ASC, e.hora ASC', (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("Ejecuto GET Eventos ")
        } else {
            console.log("ERROR GET EVENTOS: "+err);
        }
    });  
});*/

// TODOS LOS EVENTOS DE HOY (Se usa en SuscriptorEventosHoy)
app.get('/eventos/hoy', (req, res) => {
    pool.query('SELECT e.idevento, e.titulo, e.descripcion, e.fechaini, e.fechafin, e.hora, e.idarea, e.idusuario, e.obligatorio, a.area, a.idtipo FROM eventos e, areas a WHERE e.idarea = a.idarea AND CURRENT_DATE BETWEEN e.fechaini AND e.fechafin ORDER BY e.hora ASC', (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Todos los eventos de hoy /eventos/hoy")
        } else {
            console.log("ERROR: Todos los eventos de hoy /eventos/hoy"+err);
        }
    });  
});

// TODOS LOS EVENTOS DE HOY DEL PERFIL NO-LOGIN (es decir, del tablón o de los alumnos)
app.get('/eventos/nologinhoy', (req, res) => {
    pool.query('SELECT e.idevento, e.titulo, e.descripcion, e.fechaini, e.fechafin, e.hora, e.idarea, e.idusuario, e.obligatorio, a.idtipo, a.area FROM eventos e, areas a WHERE e.idarea = a.idarea AND (a.idtipo = 6 OR a.idtipo = 1) AND CURRENT_DATE BETWEEN e.fechaini AND e.fechafin ORDER BY e.hora ASC', (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Todos los eventos de hoy del perfil NoLogin /eventos/nologinhoy")
        } else {
            console.log("ERROR: Todos los eventos de hoy del perfil NoLogin /eventos/nologinhoy "+err);
        }
    });  
});
    
// TODOS LOS EVENTOS CREADOS POR UN USUARIO
app.get('/eventos/idusuario/:idusuario', (req, res) => {
    var idusuario = req.params.idusuario;
    pool.query('SELECT e.idevento, e.titulo, e.descripcion, e.fechaini, e.fechafin, e.hora, e.idarea, e.idusuario, e.obligatorio, a.area FROM eventos e, areas a WHERE e.idarea = a.idarea and e.idusuario = ' + mysql.escape(idusuario) +' ORDER BY e.fechaini ASC, e.hora ASC', (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Todos los eventos creados por un usuario /eventos/idusuario/:idusuario")
        } else {
            console.log("ERROR: Todos los eventos creados por un usuario /eventos/idusuario/:idusuario "+err);
        }
    });  
});

// EVENTO POR IDEVENTO
app.get('/evento/:idevento', (req, res) => {
    var idevento = req.params.idevento;
    pool.query('SELECT e.idevento, e.titulo, e.descripcion, e.fechaini, e.fechafin, e.hora, e.idarea, e.idusuario, e.obligatorio, a.area, a.idtipo FROM eventos e, areas a WHERE e.idarea = a.idarea and e.idevento = ' + mysql.escape(idevento), (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Evento por idevento /evento/:idevento")
        } else {
            console.log("ERROR: Evento por idevento /evento/:idevento "+err);
        }
    });  
});
    
// TODOS LOS EVENTOS DE UN SLUG DE AREA (DESDE HOY EN ADELANTE)
app.get('/eventos/:slug', (req, res) => {
    var slug = req.params.slug;
    var consulta = 'SELECT e.idevento, e.titulo, e.descripcion, e.fechaini, e.fechafin, e.hora, e.idarea, e.idusuario, e.obligatorio, a.area, a.idtipo from eventos e, areas a, tipos t where a.idarea = e.idarea and a.idtipo = t.idtipo and t.slug =' + mysql.escape(slug) +' AND e.fechafin >= CURRENT_DATE ORDER BY e.fechaini, e.hora';
    console.log(consulta);
    pool.query(consulta, (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Todos los eventos de un slug de area /eventos/:slug "+slug)
        } else {
            console.log("ERROR: Todos los eventos de un slug de area /eventos/:slug "+err);
        }
    });  
});   

    
// TODOS LOS USUARIOS
/* app.get('/usuarios', (req, res) => {
    pool.query('SELECT u.usuario, r.rol FROM usuarios u, roles r WHERE u.idrol = r.idrol', (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("Ejecuto GET Usuarios ")
        } else {
            console.log("ERROR GET USUARIOS: "+err);
        }
    });  
});*/ 
    
// USUARIO Y ROL DE USUARIO A PARTIR DEL ID DE USUARIO
app.get('/usuarios/:idusuario/rol', (req, res) => {
    var idusuario = req.params.idusuario;
    var consulta = 'SELECT u.usuario, r.rol FROM usuarios u, roles r WHERE u.idrol = r.idrol AND u.idusuario = ' + mysql.escape(idusuario);
    console.log(consulta);
    pool.query(consulta, (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Usuario y rol de usuario a partir del id de un usuario")
        } else {
            console.log("ERROR: Usuario y rol de usuario a partir del id de un usuario "+err);
        }
    });  
});    

// USUARIO A PARTIR DEL ID DE USUARIO
app.get('/usuarios/:usuario/idusuario', (req, res) => {
    var usuario = req.params.usuario;
    var consulta = 'SELECT idusuario FROM usuarios WHERE usuario = ' + mysql.escape(usuario);
    pool.query(consulta, (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Usuario a partir del id de un usuario")
        } else {
            console.log("ERROR: Usuario a partir del id de un usuario "+err);
        }
    });  
});     

// ID Y ROL DE USUARIO A PARTIR DE USUARIO Y CONTRASEÑA
app.get('/usuarios/:usuario/:password/idusuarioidrol', (req, res) => {
    var usuario = req.params.usuario;
    var password = req.params.password;
    var consulta = 'SELECT idusuario, idrol FROM usuarios WHERE usuario = ' + mysql.escape(usuario) + ' AND password like ' + mysql.escape(sha512(password));

    pool.query(consulta, (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Id y rol de usuario a partir de usuario y contraseña")
        } else {
            console.log("ERROR: Id y rol de usuario a partir de usuario y contraseña"+err);
        }
    });  
});     

// TIPOS DE AREAS QUE PUEDE CREAR O MODIFICAR UN EDITOR
app.get('/tiposareaseditables/:idusuario', (req, res) => {
    var idusuario = req.params.idusuario;
    var consulta = 'select a.idtipo from areas a, editores e where a.idarea = e.idarea and e.idusuario = ' + mysql.escape(idusuario) + 'group by a.idtipo';
    pool.query(consulta, (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Tipos de Areas de eventos que puede crear o modificar un editor /tiposareaseditables/:idusuario ")
        } else {
            console.log("ERROR: Tipos de Areas de eventos que puede crear o modificar un editor /tiposareaseditables/:idusuario"+err);
        }
    });  
});      
    
//AREAS DE EVENTOS QUE PUEDE CREAR O MODIFICAR UN EDITOR
app.get('/areaseditables/:idusuario', (req, res) => {
    var idusuario = req.params.idusuario;
    var consulta = 'SELECT e.idarea from editores e where e.idusuario = ' + mysql.escape(idusuario);
    console.log('la consulta es'+consulta);
    pool.query(consulta, (err, rows, fields) => {
        if(!err) {
            res.json(rows);
            console.log("GET: Areas de eventos que puede crear o modificar un editor /areaseditables/:idusuario ")
        } else {
            console.log("ERROR: Areas de eventos que puede crear o modificar un editor /areaseditables/:idusuario"+err);
        }
    });  
});  
    
//CREAR UN NUEVO AREA
app.post('/areas', (req, res) => {
    var consulta = 'INSERT INTO areas(area, idtipo) VALUES ("'+req.body.area+'", '+req.body.idtipo+')';
    console.log(consulta);
    pool.query(consulta, function (err, rows, fields) {
        // Si da error, es que no habia datos y enviamos error
        var codigo = new Object();
        if(!err) {
            codigo.status = 1;
            codigo.informacion = "El área se ha guardado correctamente"
            res.json(codigo);
            console.log("POST: Nuevo área /areas ")
        } else {
            if (err) {
                codigo.status = 0;
                codigo.informacion = "Ha habido un error al guardar el área"
                res.json(codigo);
                console.log("ERROR: Nuevo area /areas ")
                return;
            }
        }
    })  
});    

//MODIFICAR UN AREA
app.post('/area/', (req, res) => {
    var consulta = 'UPDATE areas SET area="'+req.body.area+'", idtipo='+req.body.idtipo+' WHERE idarea='+req.body.idarea;
    console.log(consulta);
    pool.query(consulta, function (err, rows, fields) {
        // Si da error, es que no habia datos y enviamos error
        var codigo = new Object();
        if(!err) {
            codigo.status = 1;
            codigo.informacion = "El área se ha modificado correctamente"
            res.json(codigo);
            console.log("POST: Modificar área /area/ ")
        } else {
            if (err) {
                codigo.status = 0;
                codigo.informacion = "Ha habido un error al modificar el área"
                res.json(codigo);
                console.log("ERROR: Modificar área /area/ ")
                return;
            }
        }
    })  
});      
 
//BORRAR UN ÁREA
app.delete('/area/:idarea', (req, res) => {
    //Utilizo esto para que funcione el DELETE sin dar error de Cors
    //res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    var idarea = req.params.idarea;
    var consulta = 'DELETE FROM areas WHERE idarea = ' + mysql.escape(idarea);
    console.log(consulta);
    pool.query(consulta, function (err, rows, fields) {
        var codigo = new Object();
        // Si da error, es que se ha producido el borrado y enviamos error
        if(!err) {
            codigo.status = 1;
            codigo.informacion = "El área se ha borrado correctamente"
            res.json(codigo);
            console.log("DELETE: Area /area/:idarea ")
        } else {
            if (err) {
                codigo.status = 0;
                codigo.informacion = "Ha habido un error al borrar el evento"
                res.json(codigo);
                console.log("ERROR: Evento /area/:idarea ")
                return;
            }
        }
    })  
});
    
//CREAR UN NUEVO EVENTO
app.post('/eventos', (req, res) => {
    console.log(req.body);
    //var usuario = req.params.idusuario;
    var consulta = 'INSERT INTO eventos(titulo, descripcion, idarea, idusuario, fechaini, fechafin, hora, obligatorio) VALUES ("'+req.body.titulo+'", "'+req.body.descripcion+'", '+req.body.idarea+', '+req.body.idusuario+', "'+req.body.fechaini+'", "'+req.body.fechafin+'", "'+req.body.hora+'", '+req.body.obligatorio   +')';

    console.log(consulta);
    pool.query(consulta, function (err, rows, fields) {
        var codigo = new Object();

        // Si da error, es que no habia datos y enviamos error
        if(!err) {
            codigo.status = 1;
            codigo.informacion = "El evento se ha guardado correctamente"
            //res.json(rows);
            res.json(codigo);
            console.log("POST: Nuevo evento /eventos ")
        } else {
            if (err) {
                codigo.status = 0;
                codigo.informacion = "Ha habido un error al guardar el evento"
                //var respuesta = new Object();
                //respuesta.status = "ERROR al crear un objeto POST Evento nuevo";
                //res.json(respuesta);
                res.json(codigo);
                console.log("ERROR: Nuevo evento /eventos ")
                return;
            }
        }
    })  
});

//MODIFICAR UN EVENTO
app.post('/evento/', (req, res) => {
    var consulta = 'UPDATE eventos SET titulo = "'+req.body.titulo+'", descripcion ="'+req.body.descripcion+'", idarea ='+req.body.idarea+', idusuario='+req.body.idusuario+', fechaini ="'+req.body.fechaini+'", fechafin="'+req.body.fechafin+'", hora="'+req.body.hora+'", obligatorio=1 WHERE idevento='+req.body.idevento;
    console.log(consulta);
    pool.query(consulta, function (err, rows, fields) {
        var codigo = new Object();
        // Si da error, es que no habia datos y enviamos error
        if(!err) {
            codigo.status = 1;
            codigo.informacion = "El evento se ha modificado correctamente"
            //res.json(rows);
            res.json(codigo);
            console.log("POST: Modificar evento /evento/ ")
        } else {
            if (err) {
                codigo.status = 0;
                codigo.informacion = "Ha habido un error al modificar el evento"
                res.json(codigo);
                console.log("ERROR: Modificar evento /evento/ ")
                return;
            }
        }
    })  
});
    
//BORRAR UN EVENTO
app.delete('/evento/:idevento', (req, res) => {
    //Utilizo esto para que funcione el DELETE sin dar error de Cors
    //res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    var idevento = req.params.idevento;
    var consulta = 'DELETE FROM eventos WHERE idevento = ' + mysql.escape(idevento);
    console.log(consulta);
    pool.query(consulta, function (err, rows, fields) {
        var codigo = new Object();
        // Si da error, es que se ha producido el borrado y enviamos error
        if(!err) {
            codigo.status = 1;
            codigo.informacion = "El evento se ha borrado correctamente"
            res.json(codigo);
            console.log("DELETE: Evento /evento/:idevento ")
        } else {
            if (err) {
                codigo.status = 0;
                codigo.informacion = "Ha habido un error al borrar el evento"
                res.json(codigo);
                console.log("ERROR: Evento /evento/:idevento ")
                return;
            }
        }
    })  
}); 
    
}

module.exports = appRouter;