
//CODIGO PARA COMPROBAR QUE LA API FUNCIONA

//Indicamos al servidor que cuando hay una petición GET al root de nuestra aplicación, debe escribir "Bienvenido..."
var appRouter = function (app) {
    app.get("/", function(req, res) {
      res.status(200).send("¡Bienvenido a nuestra API Restful");
    });
  }
  module.exports = appRouter;