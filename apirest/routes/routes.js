//BASADO EN EL CODIGO DE: https://blog.logrocket.com/setting-up-a-restful-api-with-node-js-and-postgresql-d96d6fc892d8

//Necesitamos para poder hacer hash 512
var sha512 = require('js-sha512');


var appRouter = function (app) {

    //Test
    //const pg = require('pg');

    //Constante de posgresql para la conexion
    const Pool = require('pg').Pool;

    // Creo un pool de conexiones PostgreSQL. Esto nos permitira tener varias conexiones pre-creadas para atender rápido
    const pool = new Pool({
        // connectionLimit: 100, //important
        host: '37.59.96.72', //ip del servidor postgresql
        port: 5432,
        user: 'proyecto',
        password: 'olakase',
        database: 'ataulfo'
        // debug: false
    });


    // Configurar cabeceras y cors -para evitar errores con app.delete-
    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
        res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
        res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
        next();
    });

    // GET Raiz
    app.get('/', (req, res) => {
        res.send("Bienvenido al control de inicencias");
        console.log("Accede a raiz");
    });

    /**
     * API de Incidencias
     * TODO: 
     *  - leer por id
     *  - leer por categoria
     *  - insertar
     *  - modificar
     */

    /** Tipos */
    // GET Tipos
    // !Con promesas
    app.get('/tipos', (req, res) => {
        pool.query('SELECT * FROM TIPOS')
            .then(rows => res.json(rows.rows))
            .catch(err => console.log(err.stack))
    });

    // POST Nuevo tipo
    app.post('/tipo', (req, res) => {
        let dml = "insert into TIPOS(tipo) values ($1)";
        if (req.body.tipo != '' && req.body.tipo != null) {
        pool.query(dml, [req.body.tipo])
            .then(rows => {
                res.json({
                    codigo: 0,
                    mensaje: 'Añadido con exito'
                });
            })
            .catch(err => {
                res.json({
                    codigo: 1,
                    mensaje: 'El tipo ya esta añadido'
                });
            })
        } else {
            res.json({
                codigo: 2,
                mensaje: 'El tipo enviado esta vacio'
            });
        }
    })

    /** Tecnicos */
    //GET Tecnicos (WIP) 
    //!Hecha con promesas
    app.get('/tecnicos', (req, res) => {
        pool.query('SELECT * from TECNICOS')
            .then(rows => res.json(rows.rows))
            .catch(err => console.log(err.stack))
    })

    app.get('/tecnico/:tecid', (req, res) => {
        let tecid = req.params.tecid;
        pool.query('SELECT * from TECNICOS where IDTECNICO = $1', [tecid], (err, rows, fields) => {
            if (!err) {
                //Funcionamiento normal
                res.json(rows.rows);
            } else {
                //Excepcion
                console.error(err.stack);
            }
        })
    })

    //POST Crear tecnico
    app.post('/tecnicos', (req, res) => {
        let dml = "insert into TECNICOS(tecnico,password) " +
            "values ($1, $2)";
        console.log(req.body.password);

        if (req.body.password != '' && req.body.password != null) {
            let datos = [req.body.tecnico, sha512(req.body.password)];

            pool.query(dml, datos)
                .then(() => {
                    res.json({
                        codigo: 0,
                        mensaje: 'Tecnico añadido con exito'
                    })
                })
                .catch(err => {
                    res.json({
                        codigo: 1,
                        mensaje: 'El tecnico introducido ya existe',
                        error: err
                    })
                })
        } else {
            res.json({
                codigo: 2,
                mensaje: 'La contraseña esta vacia'
            })
        }
    })

    //POST comprobar login
    //!Ruta basica del login
    app.post('/tecnico', (req, res) => {
        let dql = "select * from TECNICOS where TECNICO = $1 and PASSWORD = $2";
        let datos = [req.body.tecnico, sha512(req.body.password)];

        pool.query(dql, datos)
            .then(rows => {
                if (rows.rowCount > 0) {
                    res.json({
                        codigo: 0,
                        mensaje: 'Logeado con exito'
                    });
                } else {
                    res.json({
                        codigo: 1,
                        mensaje: 'Login incorrecto'
                    });
                }
            })
            .catch(err => {
                res.json({
                    codigo: 2,
                    mensaje: 'Error del servidor',
                    error: err
                });
            })

    })

    /** Estados */
    //GET Estados
    app.get('/estados', (req, res) => {
        pool.query('SELECT * from ESTADOS', (err, rows, fields) => {
            if (!err) {
                //Funcionamiento normal
                res.json(rows.rows);
            } else {
                //Excepcion
                console.error(err.stack);
            }
        })
    })

    /** Incidencias 
     * TODO:
     * - por aula
     * - por tipo
     * - por profesor
     * - creacion de incidencias
    */
    //GET Incidencias 
    app.get('/incidencias', (req, res) => {
        pool.query('SELECT incidencias.*, tipos.tipo '+
            ' from incidencias join tipos on incidencias.idtipo = tipos.idtipo; ', (err, rows, fields) => {
            if (!err) {
                //Funcionamiento normal
                res.json(rows.rows);
            } else {
                //Excepcion
                console.error(err.stack);
            }
        })
    })

    //GET Incidencia por id
    app.get('/incidencia/:incidenciaid', (req, res) => {
        let incidenciaid = req.params.incidenciaid;
        pool.query('SELECT * from INCIDENCIAS where idincidencia = $1', incidenciaid, (err, rows, fields) => {
            if (!err) {
                //Funcionamiento normal
                res.json(rows.rows);
            } else {
                //Excepcion
                console.error("Excepcion");
            }
        })
    })

    //GET Incidencia por tipo (WIP)
    app.get('/incidencias/:tipo', (req, res) => {
        let tipo = req.params.tipo;
        pool.query('SELECT * from INCIDENCIAS where IDTIPO = $1', [tipo], (err, rows, fields) => {
            if (!err) {
                //Funcionamiento normal
                res.json(rows.rows);
            } else {
                //Excepcion
                console.error("Excepcion");
            }
        })
    })

    //GET Incidencia por tipo (WIP)
    app.get('/incidencias/aula/:aula', (req, res) => {
        let aula = req.params.aula;
        pool.query('SELECT * from INCIDENCIAS where AULA = $1::text', [aula], (err, rows, fields) => {
            if (!err) {
                //Funcionamiento normal
                res.json(rows.rows);
            } else {
                //Excepcion
                console.error(err.stack);
            }
        })
    })

    //POST Creacion de incidencia
    app.post('/incidencias', (req, res) => {
        let query = 'insert into INCIDENCIAS(fecha, hora, aula, idtipo, observaciones, profesor, departamento, email, codequipo) ' +
            ' values (TIMESTAMPTZ($1::text), $2, $3, $4::integer, $5, $6, $7, $8, $9)'

        params = [];
        req.body.idtipo = parseInt(req.body.idtipo);

        for (let p in req.body) {
            params.push(req.body[p]);
            console.log(req.body[p]);
        }

        pool.query(query, params)
            .then(resp => {
                res.json({
                    codigo: 0,
                    mensaje: 'Incidencia creada con exito'
                })
            })
            .catch(err => {
                res.json({
                    codigo: 1,
                    mensaje: 'No se ha podido crear la incidencia',
                    error: err
                })
            })
    })


    /** Segimiento */
    //GET Segimientos (WIP)
    app.get('/seguimientos', (req, res) => {
        pool.query('SELECT * from SEGUIMIENTO', (err, rows, fields) => {
            if (!err) {
                //Funcionamiento normal
                res.json(rows.rows);
            } else {
                //Excepcion
                console.error("Excepcion");
            }
        })
    })

    //GET Segimientos por id
    app.get('/seguimientos/:idseg', (req, res) => {
        let idseg = req.params.idseg;
        pool.query('SELECT * from SEGUIMIENTO where IDSEGUIMIENTO = $1', [idseg], (err, rows, fields) => {
            if (!err) {
                //Funcionamiento normal
                res.json(rows.rows);
            } else {
                //Excepcion
                console.error("Excepcion");
            }
        })
    })

    //GET Segimientos por incidencia
    app.get('/seguimientos/incidencia/:id', (req, res) => {
        let id = req.params.id;
        pool.query('SELECT * from SEGUIMIENTO where IDINCIDENCIA = $1', [id], (err, rows, fields) => {
            if (!err) {
                //Funcionamiento normal
                res.json(rows.rows);
            } else {
                //Excepcion
                console.error("Excepcion");
            }
        })
    })

}
module.exports = appRouter;