
const routes = [
  {
    path: '/profesor/',
    component: () => import('layouts/LayoutProfesor.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'tipos', component: () => import('pages/Tipos.vue') },
      { path: 'incidencias', component: () => import('pages/Incidencias.vue') },
      { path: 'login', component: () => import('pages/Login.vue') }
    ]
  },
  {
    path: '/tecnico/',
    component: () => import('layouts/LayoutTecnico.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'tipos', component: () => import('pages/Tipos.vue') },
      { path: 'nuevoTipo', component: () => import('pages/NuevoTipo.vue') },
      { path: 'incidencias', component: () => import('pages/Incidencias.vue') },
      { path: 'nuevaincidencia', component: () => import('pages/NuevaIncidencia.vue') },
      { path: 'tecnicos', component: () => import('pages/Tecnicos.vue') },
      { path: 'nuevoTecnico', component: () => import('pages/NuevoTecnico.vue') },
      { path: 'seguimiento', component: () => import('pages/Seguimiento.vue') }      
    ]
  },
  {
    path: '/tecnico/editarIncidencia',
    component: () => import('layouts/LayoutTecnico.vue'),
    children: [
      { path: '', component: () => import('pages/EditarIncidencia.vue') }

    ]
  },
  {
    path: '/tecnico/nuevaIncidencia',
    component: () => import('layouts/LayoutTecnico.vue'),
    children: [
      { path: '', component: () => import('pages/NuevaIncidencia.vue') }

    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes