
const routes = [
  {
    path: '/',
    // Definimos la layout a cargar con esta ruta y el componente a meter en el layout
    component: () => import('layouts/LayoutPrincipal.vue'),
    children: [
      { path: 'estaciones', component: () => import('pages/Estaciones.vue') },
      { path: 'carril', component: () => import('pages/Carril.vue') },
      { path: 'acercade', component: () => import('pages/AcercaDe.vue') },
      
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
